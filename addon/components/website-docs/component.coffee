`import Ember from 'ember'`
`import layout from './template'`
{A, computed, inject: {service}} = Ember
{mapBy, filterBy, alias} = computed

flatten = (field) ->
  computed "#{field}.[]", ->
    output = A []
    @get(field)?.map (xs) -> output.pushObjects xs
    output

monadTapMap = (monadKey, funKey) ->
  computed "#{monadKey}.[]", funKey, ->
    fn = @get funKey
    @get(monadKey).map (x) ->
      fn(x)
      x
WebsiteDocsComponent = Ember.Component.extend
  layout: layout
  classNames: ["website-docs", "android-more-section"]
  menuLegendClass: "mdl-component website-docs__item"
  menuItemClass: "mdl-component__link website-docs__link mdl-button mdl-js-button"
  routing: service "-routing"
  sections: []
  activationCheck: computed "anchor", "routing.currentPath", ->
    anchor = @get "anchor"
    path = @get "routing.currentPath"
    (link) ->
      link.fullPath = (fullPath = [anchor, link.path].join ".")
      link.isActive = path is fullPath
  rawAllLinks: mapBy "sections", "links"
  allLinks: flatten "rawAllLinks"
  processedLinks: monadTapMap "allLinks", "activationCheck"
  activeLinks: filterBy "processedLinks", "isActive"
  activeLink: alias "activeLinks.firstObject"

`export default WebsiteDocsComponent`
