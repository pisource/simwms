`import Ember from 'ember'`
`import layout from './template'`

WebsiteFeatureCardComponent = Ember.Component.extend
  layout: layout
  classNames: ["mdl-cell", "mdl-cell--6-col", "mdl-cell--6-col-tablet", "mdl-cell--12-col-phone", "mdl-card", "mdl-shadow--3dp"]
  title: "Feature Title"

`export default WebsiteFeatureCardComponent`
