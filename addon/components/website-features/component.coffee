`import Ember from 'ember'`
`import layout from './template'`

WebsiteFeaturesComponent = Ember.Component.extend
  layout: layout
  classNames: ["website-features", "android-more-section"]
  title: "Modern features for a traditional industry"

`export default WebsiteFeaturesComponent`
