`import Ember from 'ember'`
`import layout from './template'`

WebsiteFootnavComponent = Ember.Component.extend
  tagName: "footer"
  layout: layout
  classNames: ["android-footer","mdl-mega-footer"]
  footLinkClass: "android-link mdl-typography--font-light"
  placeOfCreation: "Camarillo, CA"

`export default WebsiteFootnavComponent`
