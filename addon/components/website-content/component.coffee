`import Ember from 'ember'`
`import layout from './template'`

WebsiteContentComponent = Ember.Component.extend
  layout: layout
  classNames: ["android-content", "mdl-layout__content"]

`export default WebsiteContentComponent`
