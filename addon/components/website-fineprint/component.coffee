`import Ember from 'ember'`
`import layout from './template'`

WebsiteFineprintComponent = Ember.Component.extend
  layout: layout
  classNames: ["website-fineprint", "android-more-section"]

`export default WebsiteFineprintComponent`
