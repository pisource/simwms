`import Ember from 'ember'`
`import layout from './template'`

WebsiteHeroComponent = Ember.Component.extend
  layout: layout
  ctaClass: "mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary"

`export default WebsiteHeroComponent`
