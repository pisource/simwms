`import Ember from 'ember'`
`import layout from './template'`

WebsiteLayoutComponent = Ember.Component.extend
  layout: layout
  classNames: ["mdl-layout", "mdl-layout--no-drawer-button", "mdl-js-layout", "mdl-layout--fixed-header"]
  navLinks: []
  footLinks: []
  showDrawer: false
`export default WebsiteLayoutComponent`
