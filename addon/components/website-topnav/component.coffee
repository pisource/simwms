`import Ember from 'ember'`
`import layout from './template'`

WebsiteTopnavComponent = Ember.Component.extend
  layout: layout
  classNames: ["android-header", "mdl-layout__header", "mdl-layout__header--waterfall"]
  title: "Pisource Website"
  links: ({name, path: name} for name in ["terms", "privacy"])
  moreBtnClass: "android-more-button mdl-button mdl-js-button mdl-button--icon mdl-js-ripple-effect"
  navLinkClass: "mdl-navigation__link mdl-typography--text-uppercase"

`export default WebsiteTopnavComponent`
