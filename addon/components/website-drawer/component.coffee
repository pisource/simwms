`import Ember from 'ember'`
`import layout from './template'`

WebsiteDrawerComponent = Ember.Component.extend
  layout: layout
  classNames: ["android-drawer", "mdl-layout__drawer"]
  title: "Pisource Website"
  classNameBindings: ["showDrawer:is-visible:"]
  navLinkClass: "mdl-navigation__link"
  footLinkClass: "mdl-navigation__link"

`export default WebsiteDrawerComponent`
