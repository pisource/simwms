`import Ember from 'ember'`
`import layout from './template'`

WebsiteTermsOfServiceComponent = Ember.Component.extend
  layout: layout
  companyName: "Pisource Inc."
  serviceName: "SimWMS Warehouse Service"
  lastUpdated: "April 6, 2016"
  classNames: ["terms-of-service", "privacy-terms"]
  ageReq: 18
  governingBody: "California, United States"

`export default WebsiteTermsOfServiceComponent`
