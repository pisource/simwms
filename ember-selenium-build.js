// Here's your chance to specify what routes will be statically pre-rendered
module.exports = [
  "/",
  "terms",
  "privacy",
  "assets",
  "assets/requirements",
  "assets/layout",
  "assets/hero",
  "assets/features",
  "assets/docs",
  "assets/fineprint"
];