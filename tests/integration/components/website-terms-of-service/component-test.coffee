`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'website-terms-of-service', 'Integration | Component | website terms of service', {
  integration: true
}

test 'it renders', (assert) ->
  assert.expect 2

  # Set any properties with @set 'myProperty', 'value'
  # Handle any actions with @on 'myAction', (val) ->

  @render hbs """{{website-terms-of-service}}"""

  assert.equal @$().text().trim(), ''

  # Template block usage:
  @render hbs """
    {{#website-terms-of-service}}
      template block text
    {{/website-terms-of-service}}
  """

  assert.equal @$().text().trim(), 'template block text'
