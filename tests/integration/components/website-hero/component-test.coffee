`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'website-hero', 'Integration | Component | website hero', {
  integration: true
}

test 'it renders', (assert) ->
  assert.expect 2

  # Set any properties with @set 'myProperty', 'value'
  # Handle any actions with @on 'myAction', (val) ->

  @render hbs """{{website-hero}}"""

  assert.equal @$().text().trim(), ''

  # Template block usage:
  @render hbs """
    {{#website-hero}}
      template block text
    {{/website-hero}}
  """

  assert.equal @$().text().trim(), 'template block text'
