`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'website-fineprint', 'Integration | Component | website fineprint', {
  integration: true
}

test 'it renders', (assert) ->
  assert.expect 2

  # Set any properties with @set 'myProperty', 'value'
  # Handle any actions with @on 'myAction', (val) ->

  @render hbs """{{website-fineprint}}"""

  assert.equal @$().text().trim(), ''

  # Template block usage:
  @render hbs """
    {{#website-fineprint}}
      template block text
    {{/website-fineprint}}
  """

  assert.equal @$().text().trim(), 'template block text'
