`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'website-drawer', 'Integration | Component | website drawer', {
  integration: true
}

test 'it renders', (assert) ->
  assert.expect 2

  # Set any properties with @set 'myProperty', 'value'
  # Handle any actions with @on 'myAction', (val) ->

  @render hbs """{{website-drawer}}"""

  assert.equal @$().text().trim(), ''

  # Template block usage:
  @render hbs """
    {{#website-drawer}}
      template block text
    {{/website-drawer}}
  """

  assert.equal @$().text().trim(), 'template block text'
