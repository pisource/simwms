`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'website-layout', 'Integration | Component | website layout', {
  integration: true
}

test 'it renders', (assert) ->
  assert.expect 2

  # Set any properties with @set 'myProperty', 'value'
  # Handle any actions with @on 'myAction', (val) ->

  @render hbs """{{website-layout}}"""

  assert.equal @$().text().trim(), ''

  # Template block usage:
  @render hbs """
    {{#website-layout}}
      template block text
    {{/website-layout}}
  """

  assert.equal @$().text().trim(), 'template block text'
