`import Ember from 'ember'`
`import config from './config/environment'`

Router = Ember.Router.extend
  location: config.locationType

Router.map ->
  @route "assets", path: "/assets", ->
    @route "requirements"
    @route "layout"
    @route "hero"
    @route "features"
    @route "docs"
    @route "fineprint"

  @route "docs", path: "/docs", ->

  @route "terms"
  @route "privacy"

`export default Router`
